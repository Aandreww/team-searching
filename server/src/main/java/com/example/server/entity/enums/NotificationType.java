package com.example.server.entity.enums;

//TODO: implement types
public enum NotificationType {
    REQUEST,
    OWNERREQUEST,
    INFORMATION,
    FIREDINFO,
    LEAVEINFO,
    ACCEPTINFO,
    DECLINEINFO
}
