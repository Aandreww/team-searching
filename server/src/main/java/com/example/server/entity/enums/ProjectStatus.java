package com.example.server.entity.enums;

public enum ProjectStatus {
    OPEN,
    CLOSED,
    FULL
}
