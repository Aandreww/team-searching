export interface User {
  id: string;
  login?: string;
  password?: string;
  email: string;
  name: string;
  lastName: string;
  city: string;
  profAreas: ProfArea[];
  tags: Tag[];
  projectsCreated: String[];
  projectsParticipated: string[];
  userStatus: number;
  description: string;
}

export interface LightUser {
  id: string;
  email: string;
  name: string;
  lastName: string;
  city: string;
}

export interface Tag {
  id: string;
  name: string;
}

export interface ProfArea {
  id: string;
  name: string;
}

export interface Project {
  id: string;
  name: string;
  owner: LightUser;
  participants: LightUser[];
  tags: string[];
  projectStatus: string;
  city: string;
  profArea: string;
  description: string;
}

export interface ProjectTest {
  id: string;
  name: string;
  owner: string;
  participants: LightUser[];
  tags: string[];
  projectStatus: string;
  city: string;
  profArea: string;
  description: string;
}
