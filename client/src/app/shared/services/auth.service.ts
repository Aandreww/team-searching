import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "../interfaces";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token = null;

  constructor(private http: HttpClient) {
  }

  // TODO: understand how to work with token in spring
  login(user: User): Observable<any> {
    return this.http.post<any>('/api/users/login?username=' + user.login + '&password=' + user.password, null)
      .pipe(
        tap(() => {
          let token = localStorage.getItem('JSESSIONID');
          this.setToken(token);
        })
      );
  }

  registration(user: User): Observable<User> {
    return this.http.post<User>('/api/users/register', user);
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    if (this.token) {
      // tokenInfo = decode(this.token);
    }
    // return this.token;
    return true;
  }

  logout() {
    this.setToken(null);
    localStorage.removeItem('JSESSIONID');
  }

}
