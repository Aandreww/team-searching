import {RouterModule} from '@angular/router';
import {
  NbSidebarModule,
  NbLayoutModule,
  NbSidebarService,
  NbActionsModule,
  NbMenuModule,
  NbCardModule, NbUserModule, NbContextMenuModule, NbButtonModule
} from '@nebular/theme';
import {NgModule} from "@angular/core";
import {MainLayoutComponent} from "./main-layout.component";
import {StartPageComponent} from "../../../start-page/start-page.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    NbLayoutModule,
    NbSidebarModule,
    NbActionsModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbContextMenuModule,
    NbMenuModule
  ],
  providers: [NbSidebarService],
  declarations: [
    MainLayoutComponent,
    StartPageComponent],
  exports: [
    MainLayoutComponent
  ]
})
export class MainLayoutModule {

}
