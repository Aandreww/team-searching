import {Component, OnInit} from '@angular/core';
import {NbMenuItem, NbSidebarService} from "@nebular/theme";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.less']
})
export class MainLayoutComponent implements OnInit {

  public userItems: NbMenuItem[] = [];

  public menuItems: NbMenuItem[] = [];

  public knownUser: boolean = false;

  constructor(private sidebarService: NbSidebarService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.knownUser = this.authService.isAuthenticated();
    // this.knownUser = false;

    if (this.knownUser){
      this.userItems = [
        {
          title: 'Profile',
          icon: 'nb-person',
          link: '/profile'
        },
        {
          title: 'Logout',
          icon: 'nb-close',
          link: '/logout'
        },
      ];
      this.menuItems = [
        {
          title: 'Top',
          icon: 'nb-lightbulb',
          link: '/start'
        },
        {
          title: 'Home',
          icon: 'nb-home',
          link: '/home'
        },
        {
          title: 'Search',
          icon: 'nb-search',
          link: '/search'
        },
        {
          title: 'Mail',
          icon: 'nb-email',
          link: '/mail'
        },
        {
          title: 'Settings',
          icon: 'nb-gear',
          link: '/settings'
        }
      ];
    } else {
      this.menuItems = [
        {
          title: 'Top',
          icon: 'nb-lightbulb',
          link: '/start'
        },
        {
          title: 'Search',
          icon: 'nb-search',
          link: '/search'
        }
      ];
    }

  }

  public _toggle(): boolean{
    this.sidebarService.toggle(true);
    return false;
  }
}
