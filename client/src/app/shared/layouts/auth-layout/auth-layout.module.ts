import {RouterModule} from '@angular/router';
import {
  NbLayoutModule,
  NbCardModule, NbButtonModule, NbCheckboxModule
} from '@nebular/theme';
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {AuthLayoutComponent} from "./auth-layout.component";
import {LoginPageComponent} from "../../../components/auth/login-page/login-page.component";
import {RegistrationPageComponent} from "../../../components/auth/registration-page/registration-page.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    NbLayoutModule,
    NbCheckboxModule,
    NbCardModule,
    NbButtonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  declarations: [
    AuthLayoutComponent,
    LoginPageComponent,
    RegistrationPageComponent],
  exports: [
    AuthLayoutComponent
  ]
})
export class AuthLayoutModule {

}
