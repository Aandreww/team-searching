import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.less']
})
export class AuthLayoutComponent implements OnInit {

  constructor(public _location: Location) { }

  ngOnInit() {
  }

}
