import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../../shared/services/auth.service";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.less']
})
export class RegistrationPageComponent implements OnInit, OnDestroy {

  public form: FormGroup;

  private unsubscribe: Subject<void> = new Subject<void>();

  constructor(private authService: AuthService,
              private router: Router) {
  }

  public ngOnInit(): void {
    this.form = new FormGroup({
      login: new FormControl(null, [
        Validators.required,
        Validators.pattern("^[a-z0-9_-]{3,16}$")
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern(".+@.+..+")
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.pattern("^[a-z0-9_-]{6,18}$")
      ]),
      confirmPassword: new FormControl(null, [
        Validators.required
      ])
    }, RegistrationPageComponent.checkPasswords);
  }

  public _onSubmit(): void {
    this.form.disable();
    this.authService.registration(this.form.value)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe({
        next: () => {
          this.router.navigate(['/login'], {
            queryParams: {
              registered: true
            }
          });
        },
        error: () => this.form.enable()
      });
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  private static checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }
}
