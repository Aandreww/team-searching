import {Component, OnInit} from '@angular/core';
import {HomeServiceService} from "../services/home-service.service";
import {LightUser, ProjectTest} from "../../../shared/interfaces";

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.less']
})
export class HomePageComponent implements OnInit {

    constructor(private homeServiceService: HomeServiceService) {
    }

    ngOnInit() {
        this.homeServiceService.search("An").subscribe((users: LightUser[]) => {
            users.forEach((user: LightUser) => {
                console.log('Owner: ', user.name);
                this.homeServiceService.search2(user.id).subscribe((project: ProjectTest[]) => console.log("Project: ", project))
            })
        });
    }

}
