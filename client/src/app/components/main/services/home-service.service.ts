import {Injectable} from '@angular/core';
import {LightUser, ProjectTest} from "../../../shared/interfaces";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class HomeServiceService {

    constructor(private http: HttpClient) {
    }

    public getAllMineProjects(): ProjectTest {
        return
    }

    //todo: remove
    search(term: string): Observable<LightUser[]> {
        return <Observable<LightUser[]>>this.http
            .get(`api/users/?name=${term}`);
    }

    //todo: remove
    search2(term: string): Observable<ProjectTest[]> {
        return <Observable<ProjectTest[]>>this.http
            .get(`api/projects/?owner=${term}`);
    }

}
