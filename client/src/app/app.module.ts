import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {NbMenuModule, NbThemeModule} from "@nebular/theme";
import {HttpClientModule} from "@angular/common/http";
import {MainLayoutModule} from "./shared/layouts/main-layout/main-layout.module";
import {CommonModule} from "@angular/common";
import {AuthLayoutModule} from "./shared/layouts/auth-layout/auth-layout.module";
import {HomePageComponent} from './components/main/home-page/home-page.component';
import {SearchPageComponent} from './components/main/search-page/search-page.component';
import {MailPageComponent} from './components/main/mail-page/mail-page.component';
import {ProfilePageComponent} from './components/main/profile-page/profile-page.component';
import {SettingsPageComponent} from './components/main/settings-page/settings-page.component';
import {HttpClientInMemoryWebApiModule} from "angular-in-memory-web-api";
import {InMemoryDataService} from "./components/main/services/InMemoryDbService.service";

@NgModule({
    declarations: [
        AppComponent,
        HomePageComponent,
        SearchPageComponent,
        MailPageComponent,
        ProfilePageComponent,
        SettingsPageComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        AppRoutingModule,
        NbThemeModule.forRoot({
            name: 'corporate',
        }),
        NbMenuModule.forRoot(),
        HttpClientModule,
        AuthLayoutModule,
        MainLayoutModule,

        //todo: remove
        HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {delay: 100})
],
providers: [],
    bootstrap
:
[AppComponent]
})

export class AppModule {
}
