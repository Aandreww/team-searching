import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StartPageComponent} from "./start-page/start-page.component";
import {RegistrationPageComponent} from "./components/auth/registration-page/registration-page.component";
import {LoginPageComponent} from "./components/auth/login-page/login-page.component";
import {AuthLayoutComponent} from "./shared/layouts/auth-layout/auth-layout.component";
import {MainLayoutComponent} from "./shared/layouts/main-layout/main-layout.component";
import {AuthGuard} from "./shared/classes/auth.guard";
import {HomePageComponent} from "./components/main/home-page/home-page.component";
import {SearchPageComponent} from "./components/main/search-page/search-page.component";
import {MailPageComponent} from "./components/main/mail-page/mail-page.component";
import {ProfilePageComponent} from "./components/main/profile-page/profile-page.component";
import {SettingsPageComponent} from "./components/main/settings-page/settings-page.component";

const routes: Routes = [
  {
    path: 'auth', component: AuthLayoutComponent, children: [
      {path: 'login', component: LoginPageComponent},
      {path: 'registration', component: RegistrationPageComponent},
    ]
  },
  {
    path: '', component: MainLayoutComponent, children: [
      {path: '', redirectTo: '/start', pathMatch: 'full'},
      {path: 'start', component: StartPageComponent},
      // {path: 'start', component: StartPageComponent, canActivate: [AuthGuard]}
      {path: 'home', component: HomePageComponent},
      // {path: 'home', component: HomePageComponent, canActivate: [AuthGuard]}
      {path: 'search', component: SearchPageComponent},
      {path: 'mail', component: MailPageComponent},
      // {path: 'mail', component: MailPageComponent, canActivate: [AuthGuard]}
      {path: 'profile', component: ProfilePageComponent},
      // {path: 'profile', component: ProfilePageComponent, canActivate: [AuthGuard]}
      {path: 'settings', component: SettingsPageComponent},
      // {path: 'settings', component: SettingsPageComponent, canActivate: [AuthGuard]}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
